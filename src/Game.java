import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import Modell.Asteroid;
import Modell.Bullet;
import Modell.Shape;
import Modell.Ship;

public class Game  {

	private Display display;
	
	// Antal asteroider
	int ASTEROIDS = 40;

	// Array med asteroider
	ArrayList<Asteroid> astroidList = new ArrayList<Asteroid>();

	// Skeppet
	private Ship ship = new Ship();

	// Array av bullets
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();

	/**
	 * Konstruktor för klassen Game
	 * 
	 * @param width
	 * @param height
	 */
	public Game(int width, int height) {
		display = new Display(width, height);
		for (int i = 0; i < ASTEROIDS; i++) {

			astroidList.add(new Asteroid());
		}
		
	}

	/**
	 * Update metod
	 */
	public void update() {

		astroidList.forEach((ast) -> {

			ast.moveAsteroid();

			if (ast.getLocationX() >= 1000) {
				ast.setLocationX(0 - ast.getEdgeSize());
			}
			if (ast.getLocationY() >= 1000) {
				ast.setLocationY(0 - ast.getEdgeSize());
			}
			if (ast.getLocationX() <= 0 - ast.getEdgeSize()) {
				ast.setLocationX(1000);
			}
			if (ast.getLocationY() <= 0 - ast.getEdgeSize()) {
				ast.setLocationY(1000);
			}
		});
		
		
		try {
			CheckBulletAsteroidCollide();
			CheckCollide();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("Exception in collision checker");
		}
		
		if(!this.ship.isAlive()) {
			this.astroidList = new ArrayList<Asteroid>();
			this.ship = null;
		}

	}
	
	public void updateBullet() {
		if (this.bullets != null) {

			try {
				bullets.forEach(bullet -> {
					 bullet.moveBullet();
				});
			} catch (Exception e) {
				System.out.println("Exception in bulletarray");
				System.out.print("Exception in collision checker");
			}

		}
		
		try {
			CheckBulletAsteroidCollide();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Kollar om Bullet och Asteroid kolliderar
	 * @param bulletIterator2
	 * @param astIterator2
	 */
	private void CheckBulletAsteroidCollide() {
		
		Iterator<Bullet> bulletIterator = this.bullets.iterator();
		Iterator<Asteroid> astIterator = this.astroidList.iterator();
		
		while (bulletIterator.hasNext()) {

			Bullet bullet = bulletIterator.next();

			while (astIterator.hasNext()) {

				Asteroid ast = astIterator.next();

				if (ast.getAsteroid().getBounds().intersects(bullet.getBullet().getBounds())) {
					astIterator.remove();
					break;
				}

			}
		}
	}
	
	
	/**
	 * Kollar om skeppet och asteroider kolliderar
	 * @param astIterator3
	 */
	private void CheckCollide() {
		
		Iterator<Asteroid> astIterator = this.astroidList.iterator();
		
		while (astIterator.hasNext()) {

			Asteroid ast = astIterator.next();

			if (ast.getAsteroid().getBounds().intersects(ship.getShip().getBounds())) {

				this.ship.setAlive(false);

				System.out.print("Skepp död");
			}

		}
	}

	/**
	 * Renderingsmetod
	 */
	public void render() {

		display.render(this);
		
	}

	/**
	 * Returnerar en lista astroider
	 * 
	 * @return ArrayList<Asteroid>
	 */
	public ArrayList<Asteroid> getAstroids() {
		return astroidList;
	}

	/**
	 * Returnerar en lista bullets
	 * 
	 * @return ArrayList<Bullets>
	 */
	public ArrayList<Bullet> getBullets() {
		return this.bullets;
	}

	/**
	 * Returnerar skeppet
	 * 
	 * @return Ship
	 */
	public Ship getShip() {
		return this.ship;
	}

	/**
	 * Skapar en bullet
	 * 
	 * @param startX
	 * @param startY
	 * @param d
	 */
	public void createBullet(int startX, int startY, double angel) {
		bullets.add(new Bullet(startX, startY, angel));
	}
	


}
