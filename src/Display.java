import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import Modell.Asteroid;
import Modell.Bullet;
import Modell.Ship;

public class Display extends JFrame implements KeyListener {

	/**
	 * 
	 */

	// Grafik

	private static final long serialVersionUID = 1L;
	private Canvas canvas;
	private BufferStrategy bufferStrategy;
	private Graphics graphics;
	private Graphics2D g2d;

	// Modell

	private Game game;
	private Ship ship;

	// Data

	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();

	public Display(int width, int height) {
		setTitle("Asteroids");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.addKeyListener(this);

		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setFocusable(false);
		add(canvas);
		pack();

		canvas.createBufferStrategy(3);

		setLocationRelativeTo(null);
		setVisible(true);

	}

	public void render(Game game) {

		this.bufferStrategy = canvas.getBufferStrategy();
		this.graphics = bufferStrategy.getDrawGraphics();
		this.game = game;
		this.g2d = (Graphics2D) graphics;
		new AffineTransform();

		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, canvas.getWidth(), canvas.getWidth());

		ArrayList<Asteroid> asteroids = game.getAstroids();

		Iterator<Bullet> bulletIterator = bullets.iterator();

		graphics.setColor(Color.GRAY);

		for (Asteroid ast : asteroids) {
			if (ast.isAlive())
				g2d.fill(ast.getAsteroid());
		}

		graphics.setColor(Color.orange);
		
		this.ship = game.getShip();

		if(this.ship != null) {
			g2d.fill(ship.getShip());
		}
		else {
			Font font = new Font("GameOverFont", Font.ITALIC, 54);
			g2d.setFont(font);
			g2d.drawString("Game Over", 350, 450);
		}
		
		graphics.setColor(Color.red);
		
		try {
			if (bullets.size() != 0) {
				drawBullets(bulletIterator);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			


		graphics.dispose();

		bufferStrategy.show();
	}

	/**
	 * Ritar ut bullets 
	 * 
	 * @param bulletIterator
	 */
	private void drawBullets(Iterator<Bullet> bulletIterator) {
		while (bulletIterator.hasNext()) {

			Bullet bullet = bulletIterator.next();

			Polygon bul = bullet.getBullet();

			g2d.fill(bul);

			if (bullet.getP1().x >= canvas.getWidth() || bullet.getP1().x <= 0 || bullet.getP1().y >= canvas.getHeight()
					|| bullet.getP1().y <= 0) {
				bulletIterator.remove();
			}

		}
	}


	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_ENTER) {

			if(this.bullets.size() < 1) {
				this.game.createBullet(this.ship.getFrontX(), this.ship.getFrontY(), this.ship.getCurrentAngel());
				this.bullets = game.getBullets();
			}
		
		}

		if (e.getKeyCode() == KeyEvent.VK_D) {
			this.ship.RotateRight();
		}

		if (e.getKeyCode() == KeyEvent.VK_A) {
			this.ship.RotateLeft();
		}

		if (e.getKeyCode() == KeyEvent.VK_W) {
			this.ship.moveForward();
		}

		if (e.getKeyCode() == KeyEvent.VK_S) {
			this.ship.moveBackward();
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
