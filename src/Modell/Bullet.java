package Modell;

import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

public class Bullet implements Shape {

	private Polygon bullet;
	@SuppressWarnings("unused")
	private boolean isAlive;
	
	private double bulletAngel;

	private Point2D.Double p1 = new Point2D.Double();
	private Point2D.Double p2 = new Point2D.Double();
	private Point2D.Double p3 = new Point2D.Double();
	private Point2D.Double p4 = new Point2D.Double();

	private int width = 15;
	private int height = 5;

	private int[] bulletx;
	private int[] bullety;
	
	private int startX;
	private int startY;

	private AffineTransform at;

	/**
	 * Konstruktor för klassen Bullet
	 * @param startX
	 * @param startY
	 * @param angle
	 */
	public Bullet(int startX, int startY, double angle) {

		this.startX = startX;
		this.startY = startY;
		
		this.p1.x = startX;
		this.p1.y = startY;

		this.p2.x = p1.x + this.width;
		this.p2.y = p1.y;

		this.p3.x = p1.x + this.width;
		this.p3.y = p1.y + this.height;

		this.p4.x = p1.x;
		this.p4.y = p1.y + this.height;

		this.bulletAngel = angle;

		this.at = new AffineTransform();

		at.rotate(Math.toRadians(angle), startX, startY);
		at.transform(p1, p1);
		at.transform(p2, p2);
		at.transform(p3, p3);
		at.transform(p4, p4);

		this.bulletx = new int[] { (int) p1.x, (int) p2.x, (int) p3.x, (int) p4.x };
		this.bullety = new int[] { (int) p1.y, (int) p2.y, (int) p3.y, (int) p4.y };

		this.bullet = new Polygon(this.bulletx, this.bullety, 4);

	}

	/**
	 * Returnerar kulans vinkel
	 * 
	 * @return int
	 */
	public double getBulletAngel() {
		return bulletAngel;
	}

	/**
	 * Sätter kulans vinkel
	 * 
	 * @param bulletAngel
	 */
	public void setBulletAngel(double bulletAngel) {
		this.bulletAngel = bulletAngel;
	}

	/**
	 * Returnerar en polygon "bullet"
	 * 
	 * @return Polygon
	 */
	public Polygon getBullet() {
		return this.bullet;
	}

	/**
	 * Tar emot en Polygon bullet och sätter den
	 * 
	 * @param bullet
	 */
	public void setBullet(Polygon bullet) {
		this.bullet = bullet;
	}

	/**
	 * Metod f�r att f�r�ndra polygonens position och vinkel
	 */
	public void moveBullet() {
		
		this.p1.x = p1.x - (0.1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p1.y = p1.y + (0.1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p2.x = p2.x - (0.1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p2.y = p2.y + (0.1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p3.x = p3.x - (0.1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p3.y = p3.y + (0.1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		this.p4.x = p4.x - (0.1 * Math.sin(Math.toRadians(this.bulletAngel + 270)));
		this.p4.y = p4.y + (0.1 * Math.cos(Math.toRadians(this.bulletAngel + 270)));

		
		this.bulletx = new int[] { (int) p1.x, (int) p2.x, (int) p3.x, (int) p4.x };
		this.bullety = new int[] { (int) p1.y, (int) p2.y, (int) p3.y, (int) p4.y };

		this.bullet = new Polygon(this.bulletx, this.bullety, 4);
	}

	public Point2D.Double getP1() {
		return p1;
	}

	public void setP1(Point2D.Double p1) {
		this.p1 = p1;
	}

	@Override
	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	@Override
	public void draw(Graphics g) {
		
		System.out.print("Shape: Bullet");
		
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return (Shape) this.bullet;
	}

}
