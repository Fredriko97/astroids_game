package Modell;

import java.awt.Graphics;

public interface Shape {

	/**
	 * Metod för att kolla om objektet lever 
	 * @return True or False
	 */
	public boolean isAlive();
	/**
	 * Sätter isAlive
	 * @param True or False
	 */
	public void setAlive(boolean isAlive);
	
	/**
	 * Draw-method
	 */
	public void draw(Graphics g);
	
	/**
	 * 
	 * Metod för att generalisera objekten, satt shape kan vara asteroid, ship eller bullet etc
	 * @return Shape
	 */
	public Shape getShape();


}
