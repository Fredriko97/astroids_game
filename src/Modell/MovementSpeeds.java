package Modell;

public class MovementSpeeds {
	
	public double GetMovementSpeeds(int randomSpeedGenerator) {
		
		double movementSpeed = 0.1;
		
		if(randomSpeedGenerator == -2) {
			movementSpeed = -0.2;
		}
		else if (randomSpeedGenerator == -1) {
			movementSpeed = -0.1;
		}
		else if (randomSpeedGenerator == 0) {
			movementSpeed = 0.1;
		}
		else if (randomSpeedGenerator == 1) {
			movementSpeed = 0.1;
		}
		else if (randomSpeedGenerator == 2) {
			movementSpeed = 0.2;
		}
		
		return movementSpeed;
	}

}
