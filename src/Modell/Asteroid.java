package Modell;

import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.util.Random;

public class Asteroid implements Shape {

	private Polygon astroid;

	Random random = new Random();
	
	private int[] astroidx;
	private int[] astroidy;

	private boolean isAlive;
	
	
	private Point2D.Double p1 = new Point2D.Double();
	private Point2D.Double p2 = new Point2D.Double();
	private Point2D.Double p3 = new Point2D.Double();
	private Point2D.Double p4 = new Point2D.Double();
	
	private MovementSpeeds movementSpeeds = new MovementSpeeds();
	
	private int edgeSize;
	private int startX;
	private int startY;
	
	private double xMove, yMove;

	
	public Asteroid() {
		super();

		this.startX = random.nextInt(1000);
		
		while(this.startX >= 200 && this.startX <= 800) {
			this.startX = random.nextInt(1000);
		}
		
		this.startY = random.nextInt(1000);
		
		while(this.startY >= 200 && this.startY <= 800) {
			this.startY = random.nextInt(1000);
		}
		
		p1.x = startX;
		p1.y = startY;
	
		this.edgeSize = 20 + random.nextInt(30);
		
		p2.x = startX + edgeSize;
		p2.y = startY;
		
		p3.x = startX + edgeSize;
		p3.y = startY + edgeSize;
		
		p4.x = startX;
		p4.y = startY + edgeSize;
		
		
		this.xMove = movementSpeeds.GetMovementSpeeds(-2 + (random.nextInt(4)));
		this.yMove = movementSpeeds.GetMovementSpeeds(-2 + (random.nextInt(4)));
		
		
		this.astroidx = new int[]{(int)p1.x, (int)p2.x, (int)p3.x, (int)p4.x};
		this.astroidy = new int[]{(int)p1.y, (int)p2.y, (int)p3.y, (int)p4.y};
		
		
		this.isAlive = true;
		
	
		this.astroid = new Polygon(astroidx,astroidy,4);
	}
	
	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	
	
	public Double getLocationX() {
		return this.p1.x;
	}

	public void setLocationX(int x) {
		this.p1.x = x;
		
		this.p2.x = p1.x + this.edgeSize;
		this.p2.y = p1.y;
		
		this.p3.x = p1.x + this.edgeSize;
		this.p3.y = p1.y + this.edgeSize;
		
		this.p4.x = p1.x;
		this.p4.y = p1.y + this.edgeSize;;
		
		this.updateAstroid();
	}
	
	public Double getLocationY() {
		return this.p1.y;
	}
	
	public int getEdgeSize() {
		return edgeSize;
	}

	public void setEdgeSize(int edgeSize) {
		this.edgeSize = edgeSize;
	}

	public void setLocationY(int y) {
		this.p1.y = y;
		
		this.p2.x = p1.x + this.edgeSize;
		this.p2.y = p1.y;
		
		this.p3.x = p1.x + this.edgeSize;
		this.p3.y = p1.y + this.edgeSize;
		
		this.p4.x = p1.x;
		this.p4.y = p1.y + this.edgeSize;
		
		this.updateAstroid();
	}

	public Polygon getAsteroid() {
		return astroid;
	}
	
	public void updateAstroid() {
		this.astroid = new Polygon(new int[]{(int)p1.x, (int)p2.x, (int)p3.x, (int)p4.x}, new int[]{(int)p1.y, (int)p2.y, (int)p3.y, (int)p4.y}, 4);
	}
	
	
	public void moveAsteroid() {
		
		this.p1.x = this.p1.x + this.xMove;
		this.p1.y = this.p1.y + this.yMove;
		
		this.p2.x = p1.x + this.edgeSize;
		this.p2.y = p1.y;
		
		this.p3.x = p1.x + this.edgeSize;
		this.p3.y = p1.y + this.edgeSize;
		
		this.p4.x = p1.x;
		this.p4.y = p1.y + this.edgeSize;;
		
		this.updateAstroid();
	}

	@Override
	public void draw(Graphics g) {
		
		System.out.print("Shape: Asteroid");
		
	}

	
	@Override
	public Shape getShape() {
		return (Shape) astroid;
	}

	
}
