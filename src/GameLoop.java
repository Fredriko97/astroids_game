
public class GameLoop implements Runnable {

	private boolean running;
	private final double UPDATE_RATE = 1.0d / 500.0d;
	private final double UPDATE_BULLETRATE = 1.0d / 10000.0d;

	private Game game;

	/**
	 * Konstruktor i gameloop
	 * 
	 * @param game
	 */
	public GameLoop(Game game) {
		this.game = game;
	}

	@Override
	public void run() {
		running = true;
		double accumulator = 0;
		double accumulatorBullet = 0;
		long currentTime, lastUpdate = System.currentTimeMillis();

		while (running) {
			currentTime = System.currentTimeMillis();
			double lastRenderTimeInSeconds = (currentTime - lastUpdate) / 1000d;
			accumulator += lastRenderTimeInSeconds;
			accumulatorBullet += lastRenderTimeInSeconds;
			lastUpdate = currentTime;

			while (accumulator > UPDATE_RATE) {
				update();
				accumulator -= UPDATE_RATE;
			}
			while (accumulatorBullet > UPDATE_BULLETRATE) {
				updateBullet();
				accumulatorBullet -= UPDATE_BULLETRATE;
			}
			render();
		}
	}
	private void render() {
		game.render();
	}

	private void update() {
		game.update();
	}
	
	private void updateBullet() {
		game.updateBullet();
	}

}
